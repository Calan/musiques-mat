# Musiques Mat
## Installation
1. Télécharge le projet en *.zip*
2. Avant de dézipper l'archive, **clique droit** sur le fichier .zip, va dans l'onglet **Propriétés** et coche la case **Débloquer** puis **Ok**.
3. Dézippe l'archive. Elle contient un fichier *Musiques_Mat.ps1* et un dossier *WindowsPowershellModules*.
4. Déplace le dossier *WindowsPowershellModules* dans ton dossier *Mes Documents*.
5. Déplace le fichier *Musiques_Mat.ps1* dans ton dossier qui contient tes musiques. Ce dossier doit être organisé de la façon suivante :
```bash
DossierMusiques
├── Slash
│   ├── Apocalyptic Love
│   │   ├── Anastasia.mp3
│   │   └── Apocalyptic_Love.mp3
│   └── World on Fire
│   │   ├── Shadow_Life.mp3
│   │   └── Automatic_Overdrive.mp3
├── Ultra Vomit
│   ├── Panzer Surprise !
│   ├── Objectif: Thunes
│   └── M. Patate
└── Musiques_Mat.ps1
```

## Utilisation
1. **Clique droit** sur le fichier *Musiques_Mat.ps1* puis **Exécuter avec PowerShell**
2. Le script parcourt toute ton architecture et applique les modifications suivantes sur tous les fichiers *.mp3* : Album = nom du dossier parent, Artiste = nom du dossier "grand-parent"
3. Des petits compteurs s'affichent pour t'informer sur le nombre d'artistes, albums et musiques qu'il a croisé.
4. Enjoy ! 