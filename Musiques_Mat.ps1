﻿Import-Module taglib

$files = Get-ChildItem "." -include *.mp3 -recurse 

$compteur = 0
$artistes = @()
$albums = @()


foreach($iFile in $files) 
{ 
    $iFile.fullname    
    $mediaFile=[TagLib.File]::Create($iFile.fullname) 
    
    $albumTmp = $iFile.Directory.Name
    $artisteTmp = $iFile.Directory.Parent.Name
       
    $mediaFile.Tag.Album = $albumTmp
    $mediaFile.Tag.AlbumArtists=$artisteTmp
    $mediaFile.Save()

    if(-Not $artistes.Contains($artisteTmp)){
        $artistes += $artisteTmp
    }

    if(-Not $albums.Contains($artisteTmp + "_" + $albumTmp)){
        $albums += $artisteTmp + "_" + $albumTmp
    }
      
    $compteur ++ 
}

Write-Host "`n`nArtiste(s) traité(s) : " -ForegroundColor Yellow -NoNewline 
Write-Host "$($artistes.length)" -ForegroundColor Magenta
Write-Host "Albums(s) traité(s) : " -ForegroundColor Yellow -NoNewline 
Write-Host "$($albums.length)" -ForegroundColor Magenta
Write-Host "Musiques(s) traitée(s) : " -ForegroundColor Yellow -NoNewline 
Write-Host "$compteur" -ForegroundColor Magenta


Write-Host "`nBonne écoute mon " -ForegroundColor Yellow -NoNewline
Write-Host "giga z'bro" -ForegroundColor Red -NoNewline
Write-Host " :) !" -ForegroundColor Yellow -NoNewline
Read-Host "`n`nAppuie sur une touche pour quitter"